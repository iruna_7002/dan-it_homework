package hw01;
import java.util.Random;
import java.util.random.RandomGenerator;
import java.util.Scanner;

public class Numbers {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        libs.Output2.print(String.format("Please enter your name: "));
        String Name = sc.nextLine();
        libs.Output2.print(String.format("Let the game begin!\n"));
        libs.Output2.print(String.format("Enter your integer number from 0 to 100: "));
        int Number = sc.nextInt();

        Random Rand = new Random();
        int RandNumber = Rand.nextInt(101);
        while (Number != RandNumber) {
            if (Number > RandNumber && Number <= 100) {
                libs.Output.print(String.format("Your number is too big. Please, try again.\n"));
                Number = sc.nextInt();
            }
            if (Number < RandNumber && Number >= 0) {
                libs.Output.print(String.format("Your number is too small. Please, try again.\n"));
                Number = sc.nextInt();
            }
        }
        libs.Output2.print(String.format("Congratulations, %s!\n", Name));
    }
}
