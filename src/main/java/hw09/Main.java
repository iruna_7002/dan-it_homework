package hw09;


import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) throws ParseException {
        HashSet<String> dogHabbits = new HashSet<String>();
        dogHabbits.add("Команди");
        dogHabbits.add("Їсти");
        dogHabbits.add("Спати");

        HashSet<String> domesticCatHabbits = new HashSet<String>();
        domesticCatHabbits.add("Дряпати");
        domesticCatHabbits.add("Мурчати");
        domesticCatHabbits.add("Їсти");

        HashSet<String> roboCatHabbits = new HashSet<String>();
        roboCatHabbits.add("Підзаряджатись");
        roboCatHabbits.add("Світити лазером");
        roboCatHabbits.add("Гризти дроти");


        Dog dog = new Dog("Султан", 2, new HashSet<String> (dogHabbits));
        Fish fish = new Fish("Дорі", 1);
        DomesticCat domesticCat = new DomesticCat("Каспер", 4, 25, new HashSet<String> (domesticCatHabbits));
        RoboCat roboCat = new RoboCat("Робокіт", 10, 99, new HashSet<String> (roboCatHabbits));
        Hamster hamster = new Hamster("Нафаня", 1);

        HashSet<Pet> petMan = new HashSet<Pet>();
        petMan.add(roboCat);

        HashSet<Pet> petWoman = new HashSet<Pet>();
        petWoman.add(fish);


        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт");
        manSchedule.put(DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва");

        HashMap<String, String> womanSchedule = new HashMap<String, String>();
        womanSchedule.put(DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями");
        womanSchedule.put(DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак");


        Man man = new Man("Віталій", "Христюк", "04/02/1974", 140, petMan, manSchedule);
        Woman woman = new Woman("Людмила", "Христюк", "18/08/1975", 130, petWoman, womanSchedule);
        Child children = new Child("Ольга", "Христюк", "30/01/1996", 100);


        System.out.println("--------------------------------------");
        System.out.println("Інформація про собаку наведена нижче:");
        System.out.println("-------------------------------------");
        System.out.println(dog);
        dog.respond();
        dog.eat();

        System.out.println("------------------------------------");
        System.out.println("Інформація про рибку наведена нижче:");
        System.out.println("------------------------------------");
        System.out.println(fish);
        fish.respond();
        fish.eat();

        System.out.println("------------------------------------------");
        System.out.println("Інформація про кота робота наведена нижче:");
        System.out.println("------------------------------------------");
        System.out.println(domesticCat);
        domesticCat.doFoul();
        domesticCat.respond();
        domesticCat.eat();

        System.out.println("----------------------------------------------");
        System.out.println("Інформація про доманнього кота наведена нижче:");
        System.out.println("----------------------------------------------");
        System.out.println(roboCat);
        roboCat.doFoul();
        roboCat.respond();
        roboCat.eat();

        System.out.println("--------------------------------------");
        System.out.println("Інформація про хом'яка наведена нижче:");
        System.out.println("--------------------------------------");
        System.out.println(hamster);
        hamster.respond();
        hamster.eat();

        System.out.println("---------------------------------------");
        System.out.println("Інформація про чоловіка наведена нижче:");
        System.out.println("---------------------------------------");
        System.out.println(man);
        man.describeAge();
        man.greetPet();
        man.repairCar();

        System.out.println("------------------------------------");
        System.out.println("Інформація про жінку наведена нижче:");
        System.out.println("------------------------------------");
        System.out.println(woman);
        woman.describeAge();
        woman.greetPet();
        woman.makeup();

        System.out.println("------------------------------------");
        System.out.println("Інформація про дитину наведена нижче:");
        System.out.println("------------------------------------");
        System.out.println(children);
        children.describeAge();
        children.greetPet();
        children.describePet();
    }
}
