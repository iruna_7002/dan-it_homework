package hw09;

import java.util.Arrays;
import java.util.HashSet;

public abstract class Pet {

    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private HashSet<String> habits;


    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname, int age) {
        this(nickname, age, 0, new HashSet<>(){});
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new HashSet<>(){});
    }

    public Species getSpecies(){
        return species;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }
    public String getNickname(){
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel(){
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public HashSet<String> getHabits(){
        return habits;
    }
    public void setHabits(HashSet<String> habits) {
        this.habits = habits;
    }

    public abstract void eat();
    public abstract void respond();


    @Override
    public  boolean equals(Object obj){
        if (obj == this) return true;
        if (!(obj instanceof Pet)) return false;
        Pet pet = (Pet) obj;
        return this.species.equals(pet.species) && this.nickname.equals(pet.nickname);
    }
    @Override
    public String toString() {
        return this.species + "{nickname=" + "'" + this.nickname +  "'" + ", " + "age=" + this.age + ", " +
                "trickLevel=" + this.trickLevel + ", " + "habits=" + Arrays.asList(this.habits) + "}";
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Екземпляр %s буде выдалено!\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Екземпляр був успішно видалений.\n");
        }
    }
}
