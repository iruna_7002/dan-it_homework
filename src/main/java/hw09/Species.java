package hw09;

public enum Species {
    DOG,
    FISH,
    DOMESTICCAT,
    ROBOCAT,
    UNKNOWN,
}
