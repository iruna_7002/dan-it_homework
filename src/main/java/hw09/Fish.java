package hw09;

import java.util.HashSet;

public class Fish extends Pet {
    public Fish(String nickname, int age) {
        super(nickname, age, 0, new HashSet<String>());
        super.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("Буль - буль - буль");
    }
    @Override
    public void eat() {
        System.out.println("Було б добре, якби мені дали поїсти!");
    }
}
