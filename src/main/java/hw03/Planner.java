package hw03;

import java.util.Arrays;
import java.util.Scanner;

public class Planner {
    public static void main(String[] args) {
        Scanner Scanner = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "- Do home work!";
        scedule[1][0] = "Monday";
        scedule[1][1] = "- Go to courses; watch a film!";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "- Going to yoga; packing a suitcase!";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "- Go to Kiev; stop in Odessa!";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "- Go to University; pick up diplom!";
        scedule[5][0] = "Friday";
        scedule[5][1] = "- Go to a birthday party; buy a present!";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "- Walk in the park; go to the cinema!";


        for(; ;){
            libs.Output2.print("\nPlease, input the day of the week: ");
            String Input = Scanner.nextLine();
            Input = Input.replaceAll("\\s", "");
            String Input0 = Input.toLowerCase();
            String Day = Input0.substring(0,1).toUpperCase() + Input0.substring(1);

            if (Input0.equals("exit")) {
                System.exit(0);
            }
            switch (Day) {
                case "Sunday" -> libs.Output2.print("Your tasks for Sunday: " + scedule[0][1]);
                case "Monday" -> libs.Output2.print("Your tasks for Monday: " + scedule[1][1]);
                case "Tuesday" -> libs.Output2.print("Your tasks for Tuesday: " + scedule[2][1]);
                case "Wednesday" -> libs.Output2.print("Your tasks for Wednesday: " + scedule[3][1]);
                case "Thursday" -> libs.Output2.print("Your tasks for Thursday: " + scedule[4][1]);
                case "Friday" -> libs.Output2.print("Your tasks for Friday: " + scedule[5][1]);
                case "Saturday" -> libs.Output2.print("Your tasks for Saturday: " + scedule[6][1]);
                default -> libs.Output2.print("Sorry, I don't understand you, please try again.\n");
            }
        }

    }
}
