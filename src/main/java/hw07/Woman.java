package hw07;

import java.util.HashMap;
import java.util.HashSet;

final class Woman extends Human {
    public Woman(String name, String surname, int year, int iq, HashSet<Pet> pet, HashMap<String, String> schedule) {
        super(name, surname, year, iq, pet, schedule);
    }
    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Будеш їсти, %s ?\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Хто буде їсти?");
            }
        }
    }
    public void makeup() {
        System.out.println("Треба гарно намалюватись!");
    }
}
