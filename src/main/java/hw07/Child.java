package hw07;

import java.util.HashMap;
import java.util.HashSet;

final public class Child extends Human {
    public Child(String name, String surname, int year, HashSet<Pet> pet) {
        super(name, surname, year, 0, pet,new HashMap<String, String>());
    }
    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Пішли гратися, %s ?\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Хто піде гратись?");
            }
        }
    }
}

