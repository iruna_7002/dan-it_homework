package hw07;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;


public class FamilyTests {
    @Test
    public void testToString() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт");
        manSchedule.put(DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва");

        HashMap<String, String> womanSchedule = new HashMap<String, String>();
        womanSchedule.put(DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями");
        womanSchedule.put(DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак");

        Man father = new Man("Olexandr", "Shaevich", 1980, 120, new HashSet<>(), manSchedule);
        Woman mother = new Woman("Kseniya", "Shaevich", 1983, 110, new HashSet<>(), womanSchedule);
        Child children = new Child("Alex", "Shaevich", 2003, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(children);

        Family family = new Family(mother.getName(), father.getName(), childrenList);

        String test = "Family{father='Human{name='Olexandr' year='1980' }' mother='Human{name='Kseniya' year='1983' }' children='[Human{name='Alex' surname='Shaevich' year='2003' }]' }";
        String result = family.toString();

        Assertions.assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт");
        manSchedule.put(DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва");

        HashMap<String, String> womanSchedule = new HashMap<String, String>();
        womanSchedule.put(DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями");
        womanSchedule.put(DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак");

        Man father = new Man("Olexandr", "Shaevich", 1980, 120, new HashSet<>(), manSchedule);
        Woman mother = new Woman("Kseniya", "Shaevich", 1983, 110, new HashSet<>(), womanSchedule);
        Child children = new Child("Alex", "Shaevich", 2003, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(children);

        Family family = new Family(mother.getName(), father.getName(), childrenList);

        Assertions.assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        Assertions.assertEquals(2, family.countFamily());
        Assertions.assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);
            Assertions.fail("Індекс має бути меньше!");
            Assertions.assertEquals(2, family.countFamily());
            Assertions.assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            Assertions.assertEquals("Індекс має бути меньше!", ex.getMessage());
        }
    }


    @Test
    public void testAddChild() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт");
        manSchedule.put(DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва");

        HashMap<String, String> womanSchedule = new HashMap<String, String>();
        womanSchedule.put(DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями");
        womanSchedule.put(DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак");

        Man father = new Man("Olexandr", "Shaevich", 1980, 120, new HashSet<>(), manSchedule);
        Woman mother = new Woman("Kseniya", "Shaevich", 1983, 110, new HashSet<>(), womanSchedule);
        Child children = new Child("Alex", "Shaevich", 2003, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(children);

        Family family = new Family(mother.getName(), father.getName(), childrenList);
        Assertions.assertEquals(3, family.countFamily());

        Child children2 = new Child("Mariya", "Prudnikova", 2000, new HashSet<>());
        family.addChild(children2);

        Assertions.assertEquals(4, family.countFamily());
        Assertions.assertEquals(children2.hashCode(), family.getChildren().get(1).hashCode());
    }

    @Test
    public void testCountFamily() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт");
        manSchedule.put(DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва");

        HashMap<String, String> womanSchedule = new HashMap<String, String>();
        womanSchedule.put(DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями");
        womanSchedule.put(DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак");

        Man father = new Man("Olexandr", "Shaevich", 1980, 120, new HashSet<>(), manSchedule);
        Woman mother = new Woman("Kseniya", "Shaevich", 1983, 110, new HashSet<>(), womanSchedule);
        Child children = new Child("Alex", "Shaevich", 2003, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(children);

        Family family = new Family(mother.getName(), father.getName(), childrenList);

        Assertions.assertEquals(3, family.countFamily());
    }
}
