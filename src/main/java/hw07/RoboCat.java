package hw07;

import java.util.HashSet;

public class RoboCat extends Pet implements Foul {
    public RoboCat(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    public void doFoul() {
        System.out.println("Піду прогризу дріт!");
    }
    @Override
    public void respond() {
        System.out.println("Привіт, кожаний!");
    }

    @Override
    public void eat() {
        System.out.println("Розряжена батарея...Необхідно підзарядитись!");
    }
}
