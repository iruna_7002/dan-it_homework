package hw07;

import java.util.HashMap;
import java.util.HashSet;

final class Man extends Human {
    public Man(String name, String surname, int year, int iq, HashSet<Pet> pet, HashMap<String, String> schedule) {
        super(name, surname, year, iq, pet, schedule);
    }

    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Будеш їсти, %s ?\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Хто буде їсти?");
            }
        }
    }
    public void repairCar() {
        System.out.println("Знов ця машина зламалась! Треба знов їхати на СТО!");
    }
}
