package hw07;

public enum Species {
    DOG,
    FISH,
    DOMESTICCAT,
    ROBOCAT,
    UNKNOWN,
}
