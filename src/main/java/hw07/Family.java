package hw07;

import java.util.ArrayList;

public class Family {
    private final String mother;
    private final String father;
    private ArrayList<Child> children;
    public Family(
            String mother,
            String father,
            ArrayList<Child> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public String getMother() {
        return mother;
    }

    public String getFather() {
        return father;
    }

    public ArrayList<Child> getChildren() {
        return children;
    }
    public void addChild(Child child) {
        children.add(child);
    }
    public boolean deleteChild(int index) {
        if (index < 0) throw new IllegalStateException("Має бути позитивне значення!");
        if (index > children.size()) throw new IllegalStateException("Індекс має бути меньше!");

        Child deletedChild = children.remove(index);

        return !children.contains(deletedChild);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    @Override
    public String toString() {
        return "Family" + "{" +
                "mother='" + mother + "' " +
                "father='" + father + "' " +
                "children='" + children + "' " +
                "}";
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}

