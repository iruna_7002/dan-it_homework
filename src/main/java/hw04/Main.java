package hw04;

public class Main {
    public static void main(String[] args) {
        Pet pet = new Pet("Кіт", "Каспер", 1, 89, new String[]{"гуляти", "їсти", "спати"});
        Human mother = new Human("Людмила", "Христюк", 1974);
        Human father = new Human("Віталій", "Христюк", 48, 0, pet, new String[][]{{"понеділок ", "тренування, закінчити проєкт"}});
        Human Iryna = new Human("Ірина", "Христюк", 2000);
        Human Olga = new Human("Ольга", "Христюк", 1996);
        Family family = new Family("Людмила", "Віталій", new Human[]{Iryna, Olga}, pet);

        System.out.println("------------------------------------------------");
        System.out.println("Властивості домашнього улюбленця наведені нижче:");
        System.out.println("------------------------------------------------");
        System.out.println(pet);
        pet.eat();
        pet.respond();
        pet.foul();

        System.out.println("-----------------------------------");
        System.out.println("Інформацію про маму наведено нижче:");
        System.out.println("-----------------------------------");
        System.out.println(mother);

        System.out.println("-----------------------------------");
        System.out.println("Інформацію про тата наведено нижче:");
        System.out.println("-----------------------------------");
        System.out.println(father);

        System.out.println("------------------------------------");
        System.out.println("Інформацію про Ірину наведено нижче:");
        System.out.println("------------------------------------");
        System.out.println(Iryna);

        System.out.println("------------------------------------");
        System.out.println("Інформацію про Ольгу наведено нижче:");
        System.out.println("------------------------------------");
        System.out.println(Olga);

        System.out.println("----------------------------------------");
        System.out.println("Інформацію про всю родину наведено нижче");
        System.out.println("----------------------------------------");
        System.out.println(family);
        family.greetPet();
        family.describePet();
        System.out.println("Кількість членів родини: " + family.countFamily());
        System.out.println("Видаляємо члена родини: " + family.deleteChild(1));
        System.out.println("Кількість членів родини стала: " + family.countFamily());
        System.out.println("Додаємо нового члена родини!");
        family.addChild(new Human("Катя", "Берн", 1990));
        System.out.println("Кількість членів родини стала: " + family.countFamily());
        System.out.println(family);
    }
}
