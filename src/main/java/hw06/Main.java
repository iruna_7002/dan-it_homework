package hw06;


public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Султан", 2, new String[]{"Команди", "Їсти", "Спати"});
        Fish fish = new Fish("Дорі", 1);
        DomesticCat domesticCat = new DomesticCat("Каспер", 4, 25, new String[]{"Дряпати", "Мурчати", "Їсти"});
        RoboCat roboCat = new RoboCat("Робокіт", 10, 99, new String[]{"Підзаряджатись", "Світити лазером", "Гризти дроти"});
        Hamster hamster = new Hamster("Нафаня", 1);

        Man man = new Man("Віталій", "Христюк", 1974, 140, roboCat, new String[][]{{DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт"}, {DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва"}});
        Woman woman = new Woman("Людмила", "Христюк", 1975, 130, fish, new String[][]{{DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями"}, {DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак"}});


        System.out.println("--------------------------------------");
        System.out.println("Інформація про собаку наведена нижче:");
        System.out.println("-------------------------------------");
        System.out.println(dog);
        dog.respond();
        dog.eat();

        System.out.println("------------------------------------");
        System.out.println("Інформація про рибку наведена нижче:");
        System.out.println("------------------------------------");
        System.out.println(fish);
        fish.respond();
        fish.eat();

        System.out.println("------------------------------------------");
        System.out.println("Інформація про кота робота наведена нижче:");
        System.out.println("------------------------------------------");
        System.out.println(domesticCat);
        domesticCat.doFoul();
        domesticCat.respond();
        domesticCat.eat();

        System.out.println("----------------------------------------------");
        System.out.println("Інформація про доманнього кота наведена нижче:");
        System.out.println("----------------------------------------------");
        System.out.println(roboCat);
        roboCat.doFoul();
        roboCat.respond();
        roboCat.eat();

        System.out.println("--------------------------------------");
        System.out.println("Інформація про хом'яка наведена нижче:");
        System.out.println("--------------------------------------");
        System.out.println(hamster);
        hamster.respond();
        hamster.eat();

        System.out.println("---------------------------------------");
        System.out.println("Інформація про чоловіка наведена нижче:");
        System.out.println("---------------------------------------");
        System.out.println(man);
        man.greetPet();
        man.repairCar();

        System.out.println("------------------------------------");
        System.out.println("Інформація про жінку наведена нижче:");
        System.out.println("------------------------------------");
        System.out.println(woman);
        woman.greetPet();
        woman.makeup();

    }
}
