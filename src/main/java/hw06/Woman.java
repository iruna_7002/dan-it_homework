package hw06;

final class Woman extends Human{
    public Woman(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name, surname, year, iq, pet, schedule);
    }
    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s, давай почешу животик!\n", super.getPet().getNickname());
    }
    public void makeup() {
        System.out.println("Треба гарно намалюватись!");
    }
}
