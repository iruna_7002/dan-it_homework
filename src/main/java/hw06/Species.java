package hw06;

public enum Species {
    DOG,
    FISH,
    DOMESTICCAT,
    ROBOCAT,
    UNKNOWN,
}
