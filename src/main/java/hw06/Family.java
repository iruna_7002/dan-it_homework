package hw06;

import hw05.Human;
import hw05.Pet;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

   public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }


    public Human getMother(){
        return mother;
    }
    public void setMother(Human mother){
        this.mother = mother;
    }
    public Human getFather(){
        return father;
    }
    public void setFather(Human father){
        this.father = father;
    }
    public Human[] getChildren(){
        return children;
    }
    public void setChildren(Human[] children){
        this.children = children;
    }
    public Pet getPet(){
        return pet;
    }
    public void setPet(Pet pet){
        this.pet = pet;
    }

    public void greetPet() {
        System.out.printf("Привіт, %s \n", this.pet.getNickname());
    }
    public void describePet() {
        String trick = this.pet.getTrickLevel() <= 50 ? "майже не хитрий" : "дуже хитрий";

        System.out.printf("У мене є %s, йому %s років, він %s.\n", this.pet.getSpecies(), this.pet.getAge(), trick);
    }
    public void addChild(Human child){
        child.setFamily(this);
        this.children = Arrays.copyOf(this.children, this.children.length +1);
        this.children[this.children.length -1] = child;
    }
    public boolean deleteChild(int index){
        if (index < 0 || index >= this.children.length) return false;
        for(int i = index; i <this.children.length -1; i++){
            this.children[i] = this.children[i+1];
        }
        this.children = Arrays.copyOf(this.children, this.children.length -1);
        return true;
    }
    public int countFamily(){
        return this.children.length + 2;
    }

    @Override
    public boolean equals(Object Obj) {
        if (Obj == this) {
            return true;
        }
        if (!(Obj instanceof Family)) {
            return false;
        }

        Family other = (Family) Obj;

        if (!this.mother.equals(other.mother) || !this.father.equals(other.father)) {
            return false;
        }
        if (this.pet != null && !this.pet.equals(other.pet)) {
            return false;
        }
        if (this.children.length != other.children.length) {
            return false;
        }
        for (int i = 0; i < this.children.length; i++) {
            if (!this.children[i].equals(other.children[i])) {
                return false;
            }
        }
        return true;
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Family: \n");
        sb.append("Mother: ").append(mother.toString()).append("\n");
        sb.append("Father: ").append(father.toString()).append("\n");

        if (this.children.length > 0) {
            sb.append("Children: ");
            for (Human child : this.children){
                sb.append(child.toString()).append("\n");
            }
        }
        sb.append("Pet: ");
        if(this.pet != null)  sb.append(pet.toString()).append("\n");
        return sb.toString();
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Екземпляр %s буде выдалено!\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Екземпляр був успішно видалений.\n");
        }
    }

}
