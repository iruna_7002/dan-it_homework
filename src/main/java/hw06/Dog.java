package hw06;

public class Dog extends Pet{
    public Dog(String nickname, int age, String[] habits) {
        super(nickname, age, 0, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, господар. Я сумував!");
    }

    @Override
    public void eat() {
        System.out.println("Я зголоднів, мені потрібен корм!");
    }
}
