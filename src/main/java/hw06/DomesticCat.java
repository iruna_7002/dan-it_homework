package hw06;

public class DomesticCat extends Pet implements Foul{
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOMESTICCAT);
    }

    @Override
    public void doFoul() {
        System.out.println("Час нашкодити! Піду подряпаю шпалери! Та необхідно добре замести сліди...");
    }
    @Override
    public void respond() {
        System.out.println("Погладь мене!");
    }

    @Override
    public void eat() {
        System.out.println("Мені потрібен віскас!");
    }
}
