package hw06;

final class Man extends Human{
    public Man(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name, surname, year, iq, pet, schedule);
    }

    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s, час гуляти!\n", super.getPet().getNickname());
    }
    public void repairCar() {
        System.out.println("Знов ця машина зламалась! Треба знов їхати на СТО!");
    }
}
