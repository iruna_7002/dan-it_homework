package hw06;

public class Hamster extends Pet{
    public Hamster(String nickname, int age) {
        super(nickname, age, 0, new String[]{});
        super.setSpecies(Species.UNKNOWN);
    }

    @Override
    public void respond() {
        System.out.println("Ф-ф-ф-ф");
    }
    @Override
    public void eat() {
        System.out.println("Зараз би погризти щось!");
    }
}
