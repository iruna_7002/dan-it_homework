package hw05;

import java.util.Arrays;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }
    public Human(String name, String surname, int year) {
        this(name, surname, year, 0, null, new String[][]{});
    }

    public Human() {

    }



    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname(){
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getYear(){
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getIq(){
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public Pet getPet(){
        return pet;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }
    public String[][] getSchedule(){
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    public Family getFamily(){return family;}
    public void setFamily(Family family){this.family = family;}

    @Override
    public boolean equals(Object OBJ){
        if (this == OBJ) return true;
        if(!(OBJ instanceof Human)) return false;

        Human human = (Human) OBJ;
        return Objects.equals(year, human.year) && Objects.equals(iq, human.iq) && Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) && Objects.equals(pet, human.pet) && Objects.equals(schedule, human.schedule) && Objects.equals(family, human.family);
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Human");
        sb.append("{");
        sb.append("name='").append(name).append("' ");
        sb.append("surname='").append(surname).append("' ");
        sb.append("year='").append(year).append("' ");
        if(iq != 0) {
            sb.append("iq='").append(iq).append("' ");
        }
        sb.append("pet='").append(pet).append("' ");
        if(schedule.length != 0) {
            sb.append("schedule='").append(Arrays.deepToString(schedule)).append("' ");
        }
        sb.append("}");
        return sb.toString();
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Екземпляр %s буде выдалено!\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Екземпляр був успішно видалений.\n");
        }
    }

    public void setFamily(hw06.Family family) {
    }

    public void setFamily(hw07.Family family) {
    }
}
