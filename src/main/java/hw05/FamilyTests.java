package hw05;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class FamilyTests {
    @Test
    public void testToString() {
        Human father = new Human("Olexandr", "Shaevich", 1980);
        Human mother = new Human("Kseniya", "Shaevich", 1983);
        Human child = new Human("Alex", "Shaevich", 2003);

        Family family = new Family(mother, father, new Human[]{child}, null);

        String test = "Family{father='Human{name='Olexandr' surname='Shaevich' year='1980' }' mother='Human{name='Kseniya' surname='Shaevich' year='1983' }' children='[Human{name='Alex' surname='Shaevich' year='2003' }]' }";
        String result = family.toString();

        assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() {
        Human father = new Human("Olexandr", "Shaevich", 1980);
        Human mother = new Human("Kseniya", "Shaevich", 1983);
        Human child = new Human("Alex", "Shaevich", 2003);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        assertEquals(2, family.countFamily());
        assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);

            fail("index must be less than length");
            assertEquals(2, family.countFamily());
            assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            assertEquals("index must be less than length", ex.getMessage());
        }
    }


    @Test
    public void testAddChild() {
        Human father = new Human("Olexandr", "Shaevich", 1980);
        Human mother = new Human("Kseniya", "Shaevich", 1983);
        Human child = new Human("Alex", "Shaevich", 2003);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());

        Human child2 = new Human("Brad", "Pitt", 1970);
        family.addChild(child2);

        assertEquals(4, family.countFamily());
        assertEquals(child2.hashCode(), family.getChildren()[1].hashCode());
    }

    @Test
    public void testCountFamily() {
        Human father = new Human("Olexandr", "Shaevich", 1980);
        Human mother = new Human("Kseniya", "Shaevich", 1983);
        Human child = new Human("Alex", "Shaevich", 2003);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());
    }
}
