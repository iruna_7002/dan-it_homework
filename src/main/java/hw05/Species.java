package hw05;

public enum Species {
    CAT,
    DOG,
    FISH,
    RABBIT,
    SPIDER,
    SNAKE,
}
