package hw05;

public class Main {
    public static void main(String[] args) {
        for(int i = 0; i < 10000; i++){
            new Human("Василь", "Бондаренко", 48, 120, null, new String[][]{{DayOfWeek.MONDAY.name(), "тренування, закінчити проєкт"},
                    {DayOfWeek.TUESDAY.name(), "зібрати речі, виїхати на Київ"},
                    {DayOfWeek.WEDNESDAY.name(), "відвідати собор, зустрітись з друзями"},
                    {DayOfWeek.THURSDAY.name(), "підписати договір, замовити квитки на літак"},
                    {DayOfWeek.FRIDAY.name(), "зібрати речі, вилетіти з Києва"},
            });
        }
        Pet cat = new Pet(Species.CAT, "Каспер", 4, 25, new String[]{"дряпати", "мурчати", "їсти"});
        System.out.println(cat);
        Pet dog = new Pet(Species.DOG, "Султан");
        System.out.println(dog);
        Pet fish = new Pet(Species.FISH, "Дорі");
        System.out.println(fish);
        Pet rabbit = new Pet(Species.RABBIT, "Спаркі", 6, 54, new String[]{"нюхати", "лежати", "пригати"});
        System.out.println(rabbit);
        Pet spider = new Pet(Species.SPIDER, "Роберт");
        System.out.println(spider);
        Pet snake = new Pet(Species.SNAKE, "Джулі", 2, 99, new String[]{"кусати", "повзати", "плисти"});
        System.out.println(spider);
        Human woman = new Human("Ненсі", "Джонсонюк", 65, 125, rabbit, new String[][]{{DayOfWeek.SATURDAY.name(), "сплатити комунальні послуги, сходити на пошту"},
            {DayOfWeek.SUNDAY.name(), "ранішнє шоу, вигуляти Спаркі"}
        });
        System.out.println(woman);
    }
}
