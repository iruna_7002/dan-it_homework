package hw05;

import java.util.Arrays;

public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;


    public Pet(){}
    public Pet(Species species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Species getSpecies(){
        return species;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }
    public String getNickname(){
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel(){
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public String[] getHabits(){
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat(){
        System.out.printf("Я їм!\n");
    }
    public void respond(){
        System.out.printf("Привіт, господар. Я - " + this.nickname + ". Я сумував!\n");
    }
    public void foul(){
        System.out.printf("Потрібно добре замести сліди...\n");
    }

    @Override
    public  boolean equals(Object obj){
        if (obj == this) return true;
        if (!(obj instanceof Pet)) return false;
        Pet pet = (Pet) obj;
        return this.species.equals(pet.species) && this.nickname.equals(pet.nickname);
    }
    @Override
    public String toString() {
        return this.species + "{nickname=" + "'" + this.nickname +  "'" + ", " + "age=" + this.age + ", " +
                "trickLevel=" + this.trickLevel + ", " + "habits=" + Arrays.toString(this.habits) + "}";
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Екземпляр %s буде выдалено!\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Екземпляр був успішно видалений.\n");
        }
    }
}
